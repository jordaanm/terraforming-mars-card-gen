import * as React from 'react';
import { CorpCard } from '../components/corp-card';
import { Corporation } from '../schemas/models';
import "./corporation.css";
import { FormHandler } from '../form/form-handler';
import { CorporationFormSchema } from '../schemas/schemas';

const createCorporation = (): Corporation => {
  return {
    corpName: "New Corp",
    tags: [],
    actions: [],
    corpLogoImg: "",
    dateModified: 0,
    backgroundColor: "#FFFFFF",
    backgroundImage: "",
    startingResources: [],
    flavourText: ""
  };
}

interface CorporationScreenProps {
}

export const CorporationScreen: React.FC<CorporationScreenProps> = () => {

  const [corporation, setCorporation] = React.useState(createCorporation());  

  return (
    <div className="screen">
      <aside>
        <FormHandler<Corporation> schema={CorporationFormSchema} data={corporation} onUpdate={setCorporation} />
      </aside>
      <main>
        <CorpCard corp={corporation} />
      </main>
    </div>
  );
};