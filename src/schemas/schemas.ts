import { BasicResourceType, TagType } from './models';

export enum FieldTypes {
  Text,
  Tag,
  Resource,
  Image,
  Colour
}

export const defaultValueForFieldType = (type: FieldTypes) => {
  switch(type) {
    case FieldTypes.Text: return "";
    case FieldTypes.Colour: return "#FFFFFF";
    case FieldTypes.Image: return "";
    case FieldTypes.Tag: return TagType.Event;
    case FieldTypes.Resource: return {
      resourceType: BasicResourceType.MegaCredits,
      amount: 0,
      isProduction: false
    }
  }
}

export interface FormSchemaItem {
  key: string,
  id: string,
  label: string,
  type: FieldTypes,
  isArray: boolean
}

export const CorporationFormSchema: FormSchemaItem[] = [
  {
    key: 'corpName',
    id: 'corp-name',
    label: 'Corporation Name',
    type: FieldTypes.Text,
    isArray: false
  },
  {
    key: 'tags',
    id: 'tags',
    label: 'Tags',
    type: FieldTypes.Tag,
    isArray: true
  },
  {
    key: 'corpLogoImg',
    id: 'corpLogoImg',
    label: 'Logo',
    type: FieldTypes.Image,
    isArray: false
  },
  {
    key: 'backgroundColor',
    id: 'backgroundColor',
    label: 'BG Color',
    type: FieldTypes.Colour,
    isArray: false
  },
  {
    key: 'startingResources',
    id: 'startingResources',
    label: 'Starting Resources',
    type: FieldTypes.Resource,
    isArray: true
  },
  {
    key: 'flavourText',
    id: 'flavourText',
    label: 'Flavour Text',
    type: FieldTypes.Text,
    isArray: false
  },
  {
    key: 'backgroundImage',
    id: 'backgroundImage',
    label: 'Background Image',
    type: FieldTypes.Image,
    isArray: false
  },
];
