export interface Corporation {
  corpName: string;
  tags: TagType[];
  corpLogoImg: string;
  dateModified: number;
  backgroundColor: string;
  startingResources: StartingResource[];
  actions: Action[];
  flavourText?: string;
  backgroundImage?: string;
}

export enum TagType {
  Building = "Building",
  Space = "Space",
  Science = "Science",
  Power = "Power",
  Earth = "Earth",
  Jovian = "Jovian",
  Event = "Event",
  Animal = "Animal",
  Microbe = "Microbe",
  Plant = "Plant",
  City = "City",
  Wild = "Wild",
  Venus = "Venus",
  Moon = "Moon",
}

export interface Action {
  cost?: ActionCost,
  text: string;
}

export interface ActionCost {
  amount: number;
  resourceType: BasicResourceType;
}

export enum BasicResourceType {
  MegaCredits = "MegaCredits",
  Steel = "Steel",
  Titanium = "Titanium",
  Plants = "Plants",
  Energy = "Energy",
  Heat = "Heat",
}

export enum ExtraResourceType {
  Colony = "Colony"
}


export type ResourceType = BasicResourceType | ExtraResourceType;

export interface StartingResource {
  resourceType: ResourceType;
  amount: number;
  isProduction: boolean;
}