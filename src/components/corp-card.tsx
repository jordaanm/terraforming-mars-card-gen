import * as React from 'react';
import { Corporation } from '../schemas/models';

import './corp-card.css'

interface CorpCardProps {
  corp: Corporation
}

export const CorpCard: React.FC<CorpCardProps> = ({corp}) => {
  const { tags, corpName } = corp;
  return (
    <div className="corp-card">
      <div className="inner" style={{backgroundColor: corp.backgroundColor}}>
        <div className="corp-badge">Corporation</div>
        <section className="upper">
          <div className="corp-name">
            {corpName}
          </div>
          {tags && tags.length > 0 && <div className="tags">
            {tags.map((tag, index) => {
              return <div className={`tag-icon tag--${tag.toString().toLowerCase()}`} key={`${tag}-${index}`}></div>
            })}
          </div>}
        </section>
        <section className="lower">

        </section>
      </div>
    </div>
  );
}