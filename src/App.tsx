import * as React from 'react';
import { Alignment, Navbar, Button } from '@blueprintjs/core';
import '@blueprintjs/core/lib/css/blueprint.css';

import './App.css';
import { CorporationScreen } from './screens/corporation';

enum Views {
  CORP ="corp",
  PROJECT = "project",
  PRELUDE = "prelude"
};

function App() {

  const [view, setView] = React.useState<Views>(Views.CORP);
  const setViewTo = (value:Views) => () =>  setView(value);


  return (
    <div className="App bp3-dark">
      <header>
        <Navbar>
            <Navbar.Group align={Alignment.LEFT}>
                <Navbar.Heading>Terraforming Mars Card Prototypes</Navbar.Heading>
                <Navbar.Divider />
                <Button className="bp3-minimal" icon="home" text="Corporation"  onClick={setViewTo(Views.CORP)} />
                <Button className="bp3-minimal" icon="left-join" text="Prelude"  onClick={setViewTo(Views.PRELUDE)} />
                <Button className="bp3-minimal" icon="document" text="Project"  onClick={setViewTo(Views.PROJECT)} />
            </Navbar.Group>
        </Navbar>
      </header>
      <div className="app-body">
        {view === Views.CORP && <CorporationScreen />}
      </div>
    </div>
  );
}

export default App;
