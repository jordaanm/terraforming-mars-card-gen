import { Button, ButtonGroup, Checkbox, FileInput, FormGroup, HTMLSelect, InputGroup, NumericInput } from '@blueprintjs/core';
import * as React from 'react';
import { TagType, BasicResourceType, ExtraResourceType } from '../schemas/models';
import { FormSchemaItem, FieldTypes, defaultValueForFieldType } from '../schemas/schemas';

interface FormHandlerProps<T> {
  schema: FormSchemaItem[];
  data: T;
  onUpdate: (updatedData: T) => void;
}

export function FormHandler<T>(props: FormHandlerProps<T>) {

  const { schema, data, onUpdate } = props;
  
  const updateField = (newValue: T, key: string) => {
    const newData = {
      ...data,
      [key]: newValue
    };
    onUpdate(newData);
  };

  return (<div className="form col">
    {schema.map( (s:FormSchemaItem) => {
      const { key } = s;
      const datum = (data as any)[key];
      if(s.isArray) {
        return (
          <FormGroup label={s.label}>
            <FormArray key={s.key} fieldKey={s.key} data={datum} schema={s} updateField={updateField} />
          </FormGroup>
        )
      } else {
        return (
          <FormGroup label={s.label}>
            <FormItem key={s.key} fieldKey={s.key} data={datum} schema={s} updateField={updateField} />
          </FormGroup>
        );
      }
    })}
  </div>);
};

interface FormArrayProps {
  schema: FormSchemaItem;
  fieldKey: string
  data: any[];
  updateField: (newValue: any, key: string) => void;
}

export const FormArray: React.FC<FormArrayProps> = (props) => {
  const {schema, data, fieldKey, updateField} = props;

  const type: FieldTypes = schema.type;
  const addItem = () => {
    updateField([
      ...data,
      defaultValueForFieldType(type)
    ], fieldKey);
  }

  const removeItem = () => {
    updateField(data.slice(0, -1), fieldKey);
  }

  const updateFieldForIndex = (index: number) => (value: any) => {
    const updatedField = [...data];
    updatedField[index] = value;
    updateField(updatedField, fieldKey);
  }

  return (
    <>
      <ButtonGroup>
        <Button icon="add" onClick={() => addItem()}></Button>
        <Button icon="remove" onClick={() => removeItem()}></Button>
      </ButtonGroup>
      {data.map((datum: any, index: number) => <FormItem
        schema={schema}
        data={datum}
        key={`${fieldKey}-${index}`}
        fieldKey={fieldKey}
        updateField={updateFieldForIndex(index)}
      />)}
    </>
  )
}

interface FormItemProps {
  schema: FormSchemaItem;
  fieldKey: string
  data: any;
  updateField: (newValue: any, key: string) => void;
}

export const FormItem: React.FC<FormItemProps> = (props) => {
  const {schema, data, fieldKey, updateField} = props;
  
  const onUpdate = (value: any) => updateField(value, fieldKey);

  switch(schema.type) {
    case FieldTypes.Text:
      return <TextField data={data} schema={schema} onUpdate={onUpdate} />;
    case FieldTypes.Tag:
      return <TagField data={data} schema={schema} onUpdate={onUpdate} />;
    case FieldTypes.Image:
      return <ImageField data={data} schema={schema} onUpdate={onUpdate} />;
    case FieldTypes.Colour:
      return <ColorField data={data} schema={schema} onUpdate={onUpdate} />;
    case FieldTypes.Resource:
      return <ResourceField data={data} schema={schema} onUpdate={onUpdate} />;
  }
};

interface FieldProps {
  data: any,
  schema: FormSchemaItem,
  onUpdate: (updatedValue: any) => void;
}

export const TextField: React.FC<FieldProps> = ({data, schema, onUpdate}) => {
  return (
    <InputGroup id={schema.id} 
      placeholder={schema.label}
      value={data}
      onChange={e => onUpdate(e.target.value)}
    />
  );
};

export const TagField: React.FC<FieldProps> = ({data, schema, onUpdate}) => {
  return (
    <HTMLSelect options={Object.values(TagType)} value={data} onChange={e => onUpdate(e.target.value)} />
  );
};

export const ImageField: React.FC<FieldProps> = ({data, schema, onUpdate}) => {

  const loadFile = (e: any) => {
    if(e?.target?.files?.length > 0) {
      var reader = new FileReader();
      reader.onload = function(){
        var dataURL = reader.result;
        const imgData = dataURL?.toString();
        onUpdate(imgData);
      };
      reader.readAsDataURL(e.target.files[0]);  
    }
  };

  const clear = () => onUpdate("");

  return (
    <>
      <FileInput text="Upload Image" onChange={loadFile}/>
      <Button icon="remove" onClick={clear}></Button>
      <div>
      {Boolean(data) && <img className="image-preview" src={data} alt={`Preview for ${schema.label}`} />}
      </div>
    </>
  );
}

export const ColorField: React.FC<FieldProps> = ({data, schema, onUpdate}) => {
  
  return (
    <InputGroup
      placeholder={schema.label}
      type="color"
      value={data}
      onChange={e => onUpdate(e.target.value)}
    />
  );
};


export const ResourceField: React.FC<FieldProps> = ({data, schema, onUpdate}) => {
  
  const updateKey = (key:string) => (value: any) => onUpdate({
    ...data,
    [key]: value
  });

  const updateType = updateKey('resourceType');
  const updateAmount = updateKey('amount');
  const updateIsProd = updateKey('isProduction');

  const resourceOptions = [
    ...Object.values(BasicResourceType),
    ...Object.values(ExtraResourceType)
  ];

  return (
    <div className="resource-group">
      <HTMLSelect options={resourceOptions} value={data.resourceType} onChange={e => updateType(e.target.value)} />
      <NumericInput value={data.amount} onValueChange={updateAmount} />
      <Checkbox checked={data.isProduction} label={"Production"} onChange={updateIsProd} />
    </div>
  );
};